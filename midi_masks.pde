PImage img;
import themidibus.*; //Import the library
MidiBus myBus; // The MidiBus

import codeanticode.syphon.*;
PGraphics canvas;
SyphonServer server;

// Constants for performance
int notes = 13; // number of masks
int firstNotePitch = 24; // shifting notes to take C0
int knobsNum = 8;
int BPM = 120;
int framesPB = 12;
int beatsFromStart;

PImage pi[] = new PImage[notes];
boolean notesOn[] = new boolean[100];
int notesShift[] = new int[100];
float knobValues[] = new float[knobsNum+1];

SFD send; // object for Syphon out sending

void setup() {
	MidiBus.list(); // List all available Midi devices on STDOUT. This will show each device's index and name.
	myBus = new MidiBus(this, 0, 2); // Create a new MidiBus using the device index to select the Midi input and output devices respectively.

	for (int i=0; i<notes; i++) {
		pi[i] = loadImage("1280/"+i+".png");
	}

	size(1280, 1024, P3D);
	canvas = createGraphics(1280, 1024, P3D);

	frameRate(25); 
	
	send = new SFD(this); // new Syphon Server
	
	notesShiftInit(); // shifting notes to remove flats/sharps from toggling masks on keyboard
}

void draw() {
	background(0);
	drawMidiMasks();
	send.update(); 
}

void drawMidiMasks() {
	for (int i=0; i<notes; i++) {
		if (notesOn[i]) {
			image(pi[i], 0, 0);
		}
	}
}

void noteOn(int channel, int pitch, int velocity) {
	println("Note pitch:"+pitch);
	int pitchShifted = pitch - firstNotePitch - notesShift[pitch];
	notesOn[pitchShifted] = true;
}

void noteOff(int channel, int pitch, int velocity) {
  // Receive a noteOff
  int pitchShifted = pitch - firstNotePitch - notesShift[pitch];
  notesOn[pitchShifted] = false;
}

void controllerChange(int channel, int number, int value) {
  // Receive new value on knob modification
  // knobValues[number] = value/127.000;
}

void notesShiftInit() {
	int shift = 0;

	for (int i=26; i<55; i++) {
		switch(i % 12) {
			case 2: 
			shift++;
			break;
			case 4: 
			shift++;
			break;
			case 7: 
			shift++;
			break;
			case 9: 
			shift++;
			break;
			case 11: 
			shift++;
			break;
		}
		notesShift[i] = shift;
	}
}
