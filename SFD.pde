import codeanticode.syphon.*;
class SFD{
  public PGraphics canvas;
  public SyphonServer server;
  PApplet that;
  SFD(PApplet tthat){
    that = tthat;
    canvas = createGraphics(that.width, that.height, P3D);
    // Create syhpon server to send frames out.
    server = new SyphonServer(that, "MIDI masks");
  }
 
  void update(){
    that.loadPixels();
    canvas.loadPixels();
    arrayCopy(that.pixels, canvas.pixels);
    canvas.updatePixels();
    server.sendImage(canvas);
  }
}
